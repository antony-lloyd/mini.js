/* ******************************************************
 * Mini.js - A small, lightweight JavaScript library.
 * Version: 0.1
 *
 * Developed by: Antony Lloyd - antony-lloyd.com
 * ******************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Antony Lloyd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *********************************************************/

function Mini(selector)
{
	if(window === this)
		return new Mini(selector);

	if(selector != null)
	{
		if(selector == "document")
			this.elem								= document;
		else if(selector instanceof HTMLElement)
			this.elem 								= selector;
		else
		{
			try
			{
				this.elems							= document.querySelectorAll(selector);

				this.elem = this.elems[0];
			}
			catch(e)
			{
				this.elem 							= selector;
			}
		}

		if(this.elem === null || this.elem === undefined)
			throw `Cannot find selector '${selector}`;
	}
	return this;
}

/**
 * Parses a functions options
 * @param array defaults The allowed and default values
 * @param array options The function options
 */
Mini.parseOptions 									= function(defaults, options)
{
	if(options !== undefined)
	{
		for(var option in options)
		{
			if(option in defaults)
			{
				defaults[option] 					= options[option];
			}
		}
	}

	return defaults;
}

// ----- Prototype Functions -----

Mini.prototype = {
	/////////////////////////////////////////
	/////////////////////////////////////////
	/////////////// Animations //////////////
	/////////////////////////////////////////
	/////////////////////////////////////////

	/**
	 * Fades out an elemsent
	 * @param array options Options to provide the function
	 */ 
	fadeOut 										: function(options) {
		// The default options
		var optionValues							= {
			speed 			: 25,
			callback 		: null,
			callbackArgs 	: null
		};

		optionValues 								= Mini.parseOptions(optionValues, options);

		// The starting opacity
		var opacity 								= 9;

		var elem 									= this.elem;

		if(this.elem == null)
			return;

		function fade()
		{
			// Decrease the opacity
			elem.style.opacity 						= "0." + opacity;
			opacity--;

			// If there is full opacity, stop the fade & interval
			if(opacity == -1)
			{
				window.clearInterval(doFade);

				elem.style.display					= "none";

				if(optionValues["callback"] !== null)
				{
					if(optionValues["callbackArgs"] !== null)
	        			optionValues["callback"](optionValues["callbackArgs"]);
	        		else
	          			optionValues["callback"]();
        		}
			}
		}

		var doFade 									= window.setInterval(fade, optionValues["speed"]);
	},

	/**
	 * Fades in an elemsent
	 * @param array options Options to provide the function
	 */
	fadeIn 											: function(options) {
		// The default options
		var optionValues							= {
			speed 			: 25,
			callback 		: null,
			callbackArgs 	: null
		};

		optionValues 								= Mini.parseOptions(optionValues, options);

		// The starting opacity
		var opacity 								= 0;

		if(this.elem == null)
			return;

		// If it is display block, don't fade in
		if(this.elem.style.display === "block")
			return;

		// Set opacity to 0 & show the elemsent
		this.elem.style.opacity 						= 0;
		this.elem.style.display 						= "block";

		var elem 									= this.elem;

		function fade()
		{
			// Increase the opacity
			if(opacity == 10)
	            elem.style.opacity 					= 1;
	        else
	            elem.style.opacity 					= "0." + opacity;

	        opacity++;

	        // If there is no opacity, clear the interval
	        if(opacity == 11)
	        {
	            window.clearInterval(doFade);
	            if(optionValues["callback"] !== null)
				{
					if(optionValues["callbackArgs"] !== null)
	            		optionValues["callback"](optionValues["callbackArgs"]);
	            	else
						optionValues["callback"]();
	            }
	        }
		}

		var doFade 									= window.setInterval(fade, optionValues["speed"]);
	},

	/////////////////////////////////////////
	/////////////////////////////////////////
	///////////////// Events ////////////////
	/////////////////////////////////////////
	/////////////////////////////////////////

	/**
	 * Attatches an event listerner to an elemsent
	 * @param string event The event to listen
	 * @param object func The function to call
	 * @param bool capture Uses capture
	 */
	addListener 									: function(event, func, capture) {
		this.elem.addEventListener(event, func, capture)
	},

	/**
	 * Removes an event listener from an elemsent
	 * @param string event The event to remove
	 * @param object func The function to remove
	 */
	removeListener 									: function(event, func) {
		this.elem.removeEventListener(event, func)
	},

	/**
	 * Attatches a click event to the selector
	 * @param object callback The callback to call on click
	 */
	click 											: function(callback) {
		if(callback !== null && callback !== undefined)
			this.addListener("click", callback);
		else
			this.elem.dispatchEvent(new Event("click"));
	},

	/**
	 * Attatches a change event to the selector
	 * @param object callback The callback to call on change
	 */
	change 											: function(callback) {
		if(callback !== null && callback !== undefined)
			this.addListener("change", callback);
		else
			this.elem.dispatchEvent(new Event("change"));
	},

	/**
	 * Attatches a keyrelease event to the selector
	 * @param object callback The callback to call on keyrelease
	 */
	keyrelease 										: function(callback) {
		if(callback !== null && callback !== undefined)
			this.addListener("keyup", callback);
		else
			this.elem.dispatchEvent(new Event("keyup"));
	},

	/**
	 * Attatches a submit event if callback provided, otherwise triggers a submit
	 * @param object callback The submit event (if attatching)
	 */
	submit 												: function(callback) {
		if(callback !== null && callback !== undefined)
			this.addListener("submit", callback);
		else
			this.elem.dispatchEvent(new Event("submit"));
	},

	/**
	 * Attathes a content loaded event to the selector
	 * @param object callback The callback to call on load
	 */
	ready 											: function(callback) {
		this.elem.addEventListener("DOMContentLoaded", callback)
	},

	/**
	 * Toggles a class on a selector
	 * @param string classname The class to toggle
	 */
	toggleClass 									: function(classname) {
		this.elem.classList.toggle(classname)
	},

	/**
	 * Checks is a elemsent has a class
	 * @param string classname The class to check for
	 */
	hasClass 										: function(classname) {
		this.elem.classList.contains(classname)
	},

	/**
	 * Removes an elemsent from the DOM
	 */
	remove 											: function() {
		this.elem.remove()
	},

	/**
	 * Gets the next child node
	 */
	next 											: function() {
		Mini(this.elem.nextElementSibling)
	},

	/**
	 * Gets the previous child node
	 */
	prev 											: function() {
		Mini(this.elem.previousElementSibling)
	},

	/**
	 * Gets the parent node
	 */
	parent 											: function() {
		Mini(this.elem.parentNode)
	},

	/**
	 * Gets the parent with a given class
	 * @param string className The class name
	 * @return object The parent object
	 */
	parentOfClass									: function(className) {
		while
		(
			(this.elem = this.elem.parentElement) 
			&& 
			!((this.elem.matches || this.elem.matchesSelector).call(this.elem, className))
		);
    	
    	return Mini(this.elem);
	},

	/**
	 * Gets all elemsents that have an attribute within a parent
	 * @param string attribute The attribue to search for
	 * @param string value The value of the attribute
	 * @return array An array of elemsents
	 */
	getElementsByAttributeValue 					: function(attribute, value) {
		var elemsents 								= [ ];

		var all 									= this.elem.getelemsentsByTagName("*");
		for(i = 0; i < all.length; i++)
		{
			if(all[i].getAttribute(attribute) != null)
			{
				if(all[i].getAttribute(attribute) == value)
					elemsents.push(all[i]);
			}
		}

		return elemsents;
	},

	/**
	 * Gets all elemsents that have a tag name within a parent
	 * @param string tag The tag to search for
	 * @param array An array of elemsents
	 */
	getElementsByTagName 								: function(tag) {
		this.elem.getElementsByTagName(tag)
	},

	/**
	 * Get the length of the selector
	 * @return int The length of the selector
	 */
	count 												: function() {
		this.e.length
	},

	/**
	 * Gets or sets the value of a elemsent
	 * @param string value If provided, will be set as the elemsent value
	 * @return string The value of the elemsent
	 */
	value 												: function(value) {
		if(value !== null && value !== undefined)
			this.elem.value 							= value;

		return this.elem.value;
	},

	/**
	 * Gets or sets the inner HTML of an elemsent
	 * @param string html If provided, will be set as the HTML of the element
	 * @return string The element HTML
	 */
	html 												: function(html) {
		if(html !== null && html !== undefined)
			this.elem.innerHTML 						= html;

		return this.elem.innerHTML;
	},

	/**
	 * Gets or sets the outerHTML of an element
	 * @param string html If provided, will be set as the outerHTML of the element
	 * @return string The element outerHTML
	 */
	outerHtml 											: function(html) {
		if(html !== null && html !== undefined)
			this.elem.outerHTML 						= html;

		return this.elem.outerHTML;
	},

	/**
	 * Gets or sets the inner text of an elemsent
	 * @param string text If provided, will be set as the inner text of the elemsent
	 * @return string The elemsent inner text
	 */
	text 	 											: function(text) {
		if(text !== null && test !== undefined)
			this.elem.innerText 						= text;

		return this.elem.innerText;
	},

	/**
	 * Gets or sets the tag name. Setting the tag name will re-create the element, loosing all event handlers
	 * @param string name The tag name to set
	 * @return string The tag name
	 */
	tagName 											: function(name) {
		if(name !== null && name !== undefined)
		{
			var newElem 								= document.createElement(name);

			// Copy children
			while(this.elem.firstChild)
				newElem.appendChild(this.elem.firstChild);

			// Copy attributes
			for(i = 0; i < this.elem.attributes.length; i++)
				newElem.attributes.setNamedItem(this.elem.attributes[i].cloneNode());

			this.elem.parentNode.replaceChild(newElem, this.elem);
		}

		return this.elem.tagName;
	},

	/**
	 * Gets or sets an attribute of an elemsent
	 * @param string attr The attribute to get or set
	 * @param string value If provided, will be set as the attribute value
	 * @return string The attribute value
	 */
	attr 												: function(attr, value) {
		if(value !== null && value !== undefined)
			return this.elem.setAttribute(attr, value);

		return this.elem.getAttribute(attr);
	},

	/**
	 * Appends a elemsent to an elemsent
	 * @param DOMObject e The elemsent to append
	 */
	append 												: function(e) {
		this.elem.appendChild(e)
	},

	/**
	 * Loops through an array of selectors
	 */
	each 												: function(callback) {
		for(i = 0; i < this.elems.length; i++)
			callback(this.elems[i]);
	},

	/**
	 * Sets a CSS property of an object
	 * @param string/object optOrProp Either a property name if used with value, or on its own as a object { <property> : <value> }
	 * @param string value The value of the property
	 */
	css 												: function(optOrProp, value) {
		// If optOrProp is a object, it is a list of properties
		if(typeof optOrProp == "object")
		{
			for(opt in optOrProp)
				this.elem.style.cssText 				= opt + ":" + optOrProp[opt];
		}
		else
			this.elem.style.cssText 					= optOrProp + ":" + value
	},
	
	/**
	 * Gets the HTML element
	 * @returns {*}
	 */
	get 												: function() {
		return this.elem;
	}
};

// ----- Functions that do not require an elemsent
/**
 * Perform an AJAX request
 * @param string url The URL of the request
 * @param mixed data A GET string or a HTML form object
 * @param bool async Send the request asyncously
 */
Mini.ajax 												= (url, data = null, async = true) => {
	var method = "GET";
	var urlData = false;

	if(typeof data === "string")
		urlData = true;
	else if(typeof data === "object")
	{
		method = "POST";
		data = new FormData(data);
	}
	else if(data !== null)
		throw "Mini: AJAX data format is invalid";

	return new Promise(function(success, fail) {
		var request;

		if(window.XMLHttpRequest)
			request = new XMLHttpRequest();
		else if(window.ActiveXObject) // IE6 & older
			request = new ActiveXObject("Microsoft.XMLHTTP");

		request.open(method, url, async);

		request.onload = () => {
			var a = JSON.parse(request.responseText);
			success({
				status : request.status,
				data : a
			});
		};

		request.onerror = () => {
			fail(new Error("Mini: AJAX network fail"));
		};

		if(urlData)
			request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		request.send(data);
	});
};

/**
 * Creates a HTML elemsent based on a HTML string
 * @param string html The HTML to create
 * @return DOMelemsent The HTML elemsent
 */
Mini.create 											= (html) => {
	var parts 											= html.replace('>', ' ').split(" ");

	var tag 											= document.createElement(parts[0].substr(1));

	var elemsParts 										= html.substr(parts[0].length + 1).replace(/=/g, "").split("'");

	// Loop through each elemsent part, creating the attribute
	for(index = 0; index < elemsParts.length; index++)
	{
		var attrName 									= elemsParts[index].trim();

		// Check if it is the end tag
		if(index == elemsParts.length - 1 || elemsParts[index][0] == '>')
		{
			// Check if there is any innerHTML
			if(attrName != "/>")
				tag.innerHTML 							= attrName.substr(1, attrName.length - (parts[0].length + 3));
		}
		else
		{
			// If the index is true, it is an attribute type
			if(index % 2 == 0)
			{
				var attrValue 							= elemsParts[(index + 1)].trim();

				tag.setAttribute(attrName, attrValue);

				index++;
			}
		}
	}

	return tag;
};

/**
 * Scrolls the page to a given point
 * @param int to The to Y pos
 */
Mini.scroll 									= (to) => {
	var start 									= self.pageYOffset;
	var end 									= to;
	var distance 								= end > start ? end - start : start - end;

	var speed 									= Math.round(distance / 100);
	if(speed < 20)
		speed = 20;

	var step 									= Math.round(distance / 25);
	var leap 									= end > start ? start + step : start - step;
	var timer 									= 0;

	if(end > start)
	{
		for(var i = start; i < end; i += step)
		{
			setTimeout(window.scrollTo.bind(null, 0, leap), (timer * speed));
			leap += step;
			if(leap > end)
				leap = end;
			timer++;
		}
	}
	else
	{
		for(var i = start; i > end; i -= step)
		{
			setTimeout(window.scrollTo.bind(null, 0, leap), (timer * speed));
			leap -= step;
			if(leap < end)
				leap = end;
			timer++;
		}
	}
};

/* *** Convert timestamp *** */
Mini.convertTimestamp 									= (timestamp) => new Date(timestamp * 1000);